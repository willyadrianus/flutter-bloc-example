import 'package:flutter_bloc_example/validator/validators.dart';
import 'package:flutter_test/flutter_test.dart';

void main(){
  test("Error Empty Validator", (){
    expect(Validators.isNotEmpty(null), false);
    expect(Validators.isNotEmpty(""), false);
  });

  test("Error Length Equal Than Validator", () {
    expect(Validators.isLengthEqualThan(null, 3), false);
    expect(Validators.isLengthEqualThan("", 3), false);
    expect(Validators.isLengthEqualThan("Hi", 3), false);
  });
}