import 'package:flutter/material.dart';

import '../util/font_size.dart';

class SnackBarBuilder extends SnackBar{

  final String message;
  final String actionLabel;
  final int durationInSeconds;
  SnackBarBuilder({
    @required this.message,
    @required this.actionLabel,
    this.durationInSeconds = 3
  }) : super(
    duration: Duration(seconds: durationInSeconds),
    content: Text(
      message,
      style: TextStyle(
        color: Colors.white,
        fontSize: FontSize.standard,
      ),
    ),
    action: SnackBarAction(
      label: actionLabel,
      onPressed: (){},
    ),
  );

}