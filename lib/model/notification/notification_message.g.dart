// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationMessage _$NotificationFromJson(Map<String, dynamic> json) {
  return NotificationMessage(
    id: json['id'] as int,
    title: json['title'] as String,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$NotificationToJson(NotificationMessage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'message': instance.message,
    };
