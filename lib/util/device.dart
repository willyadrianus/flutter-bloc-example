import 'dart:ui';

/// Device is Util
/// Berisi informasi ukuran device
///
/// Readme:
/// Wajib "initialize()" saat halaman awal
///
/// Example:
/// Device.initialize(MediaQuery.of(context).size);

class Device{

  static double width, height;

  static initialize(Size size){
    width = size.width;
    height = size.height;
  }

  static getMaxSize() => (width > height) ? width : height;
}