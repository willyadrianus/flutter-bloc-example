import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter_bloc_example/model/response.dart';
import 'package:http/http.dart' as http;

class Service {
  final http.Client client = http.Client();

  static const String SERVER_URL = "http://10.120.120.30:8080"; // Local

  static const int DEFAULT_TIMEOUT_IN_SECONDS = 30;
  static const int TIMEOUT_FOR_UPLOADING_IMAGE = 100;

  static const String PATH_RELOADED = "/rest/api/reloaded";
  static const String PATH_SERVICE = PATH_RELOADED + "/invoke";
  static const String PATH_BINARY = "/bin?res=";

  static String get getBinaryPath => SERVER_URL + PATH_BINARY;

  static Uri get getDefaultWriteUri => Uri.parse(SERVER_URL + PATH_SERVICE);

  static Uri get getDefaultReloadedUri => Uri.parse(SERVER_URL + PATH_RELOADED);

  Future<Response<dynamic>> getRequest(String url) async {
//    final token = await FirebaseService().getAuthToken();
    final token = {
      "token": "T0ken",
    };
    try {
      final http.Response response = await client
          .get(url, headers: token)
          .timeout(Duration(seconds: DEFAULT_TIMEOUT_IN_SECONDS));
      if (response.statusCode == HttpStatus.ok)
        return responseOK(response);
      else
        return responseError(response);
    } on TimeoutException catch (e) {
      return responseTimeout(e);
    } on SocketException catch (e) {
      return responseSocket(e);
    } on FormatException catch (e) {
      return responseErrorException(e);
    } catch (e) {
      return Response<dynamic>.createErrorResponse(
          "(#Unknown Error)" + "Gagal mengambil data");
    }
  }

  Future<Response<dynamic>> postRequest(String encodedJson) async {
    Uri url = (encodedJson.contains('model'))
        ? getDefaultReloadedUri
        : getDefaultWriteUri;
//    final token = await FirebaseService().getAuthToken();
    final token = {
      "token": "T0ken",
    };
    try {
      final http.Response response = await client
          .post(url, body: encodedJson, headers: token)
          .timeout(Duration(seconds: DEFAULT_TIMEOUT_IN_SECONDS));
      if (response.statusCode == HttpStatus.ok)
        return responseOK(response);
      else
        return responseError(response);
    } on TimeoutException catch (e) {
      return responseTimeout(e);
    } on SocketException catch (e) {
      return responseSocket(e);
    } on FormatException catch (e) {
      return responseErrorException(e);
    } catch (e) {
      return Response<dynamic>.createErrorResponse(
          "(#Unknown Error)" + "Gagal mengambil data");
    }
  }

  Response<dynamic> responseOK(http.Response response) {
    final decodedJson = json.decode(response.body);
    if (decodedJson != null && decodedJson.isNotEmpty) {
      return Response<dynamic>(
        httpStatusCode: response.statusCode,
        result: Response.REQUEST_SUCCESS,
        data: (decodedJson is List<dynamic>)
            ? decodedJson
            : List.of([decodedJson]),
      );
    } else {
      return Response<dynamic>(
        httpStatusCode: response.statusCode,
        result: Response.REQUEST_SUCCESS_EMPTY,
      );
    }
  }

  Response<dynamic> responseError(http.Response response) {
    final decodedJson = json.decode(response.body);
    if (decodedJson != null) {
      final errorMap = (decodedJson as Map<String, dynamic>);
      if (errorMap.containsKey("message"))
        return Response<dynamic>.createErrorResponse(
            "(#${response.statusCode})" + errorMap["message"]);
      else if (errorMap.containsKey("stacktrace"))
        return Response<dynamic>.createErrorResponse(
            "(#${response.statusCode})" + errorMap["stacktrace"]);
      else
        return Response<dynamic>.createErrorResponse(
            "(#${response.statusCode})" + "Gagal mengambil data");
    } else
      return Response<dynamic>.createErrorResponse(
          "(#${response.statusCode})" + "Gagal mengambil data");
  }

  Response<dynamic> responseTimeout(TimeoutException e) {
    return Response<dynamic>.createErrorResponse(e.message);
  }

  Response<dynamic> responseSocket(SocketException e) {
    return Response<dynamic>.createErrorResponse(e.message);
  }

  Response<dynamic> responseErrorException(FormatException e) {
    return Response<dynamic>.createErrorResponse(e.message);
  }
}
