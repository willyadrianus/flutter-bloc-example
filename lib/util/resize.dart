import 'device.dart';

/// Resize is Util
/// digunakan untuk mendapatkan ukuran sesuai tinggi/lebar device
/// [_field] dapat diubah sesuai kebutuhan
/// [_standardSize] adalah ukuran standard device / emulator -
/// - yang digunakan saat develop
/// [_minSize] dan [_maxSize] adalah ukuran minimal dan maksimal
///
/// Test:
/// "calculateTestDifferentDevice()" digunakan untuk test size dengan ukuran -
/// - device yang berbeda
///
/// Dependency:
/// Device.dart

class Resize {

  static final double _standardSize = 700;
  static final double _minSize = 0;
  static final double _maxSize = 999999999;

  static double of(double value, {double minSize, double maxSize}){
    if (value == 0) return value; // return 0 if value is 0
    if (minSize == null) minSize = _minSize;
    if (maxSize == null) maxSize = _maxSize;

    double getRatio = _standardSize / value;
    double calculatedValue = Device.getMaxSize() / getRatio;

    if (minSize > calculatedValue) return minSize;
    else if (maxSize < calculatedValue) return maxSize;
    else return calculatedValue;
  }

  static double value(double value, {double minSize, double maxSize}) => of(value, minSize: minSize, maxSize: maxSize);

  // For Testing in Different Device
  static double calculateTestDifferentDevice(double value, double deviceSize, {double minSize, double maxSize}){
    if (minSize == null) minSize = _minSize;
    if (maxSize == null) maxSize = _maxSize;
    if (deviceSize == null) deviceSize = _standardSize;

    double getRatio = deviceSize / value;
    double calculatedValue = Device.getMaxSize() / getRatio;

    if (minSize > calculatedValue) return minSize;
    else if (maxSize < calculatedValue) return maxSize;
    else return calculatedValue;
  }

}