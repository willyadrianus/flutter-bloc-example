import 'package:flutter_bloc_example/bloc/notification_form/notification_form_bundle.dart';
import 'package:flutter_bloc_example/model/notification/notification_message.dart';
import 'package:flutter_bloc_example/model/response.dart';
import 'package:flutter_bloc_example/repository/notification_repository.dart';
import 'package:mockito/mockito.dart';
import 'package:test_api/test_api.dart';

class NotificationRepositoryMock extends Mock
    implements NotificationRepository {}

void main() {
  group("Notification Form Bloc", () {
    NotificationFormBloc notificationFormBloc;
    NotificationRepository notificationRepository;
    NotificationMessage _mockResultNotification = NotificationMessage(
      id: 1,
      title: "Hi Apa Kabar",
      message: "Test aja",
    );

    NotificationMessage _mockInputNotification = NotificationMessage(
      title: "Hi Apa Kabar",
      message: "Test aja",
    );

    setUp(() {
      notificationRepository = NotificationRepositoryMock();
      notificationFormBloc =
          NotificationFormBloc(notificationRepository: notificationRepository);
    });

    test("Initial State is InitializeNotificationForm", () {
      expect(
        notificationFormBloc.initialState,
        InitializeNotificationForm(
          isTitleValid: true,
          isMessageValid: true,
        ),
      );
    });

    test("Title Changed Event", () async {
      await Future.delayed(Duration(milliseconds: 400));
      notificationFormBloc.dispatch(TitleChanged(title: ""));
      await Future.delayed(Duration(milliseconds: 400));
      notificationFormBloc.dispatch(TitleChanged(title: "H"));
      await Future.delayed(Duration(milliseconds: 400));
      notificationFormBloc.dispatch(TitleChanged(title: "Hi Apa Kabar"));

      expectLater(
        notificationFormBloc.state,
        emitsInOrder([
          InitializeNotificationForm(isTitleValid: false, isMessageValid: true),
          InitializeNotificationForm(isTitleValid: true, isMessageValid: true),
        ]),
      );
    });

    test("Message Changed Event", () async {
      await Future.delayed(Duration(milliseconds: 400));
      notificationFormBloc.dispatch(MessageChanged(message: ""));
      await Future.delayed(Duration(milliseconds: 400));
      notificationFormBloc.dispatch(MessageChanged(message: "T"));
      await Future.delayed(Duration(milliseconds: 400));
      notificationFormBloc.dispatch(MessageChanged(message: "Test aja"));

      expectLater(
        notificationFormBloc.state,
        emitsInOrder([
          InitializeNotificationForm(isTitleValid: true, isMessageValid: false),
          InitializeNotificationForm(isTitleValid: true, isMessageValid: true),
        ]),
      );
    });

    group("SubmitNotificationPressed Event", () {
      test("Success", () {
        when(notificationRepository.createNotification(_mockInputNotification))
            .thenAnswer((_) => Future.value(
                Response.createSuccessResponse(_mockResultNotification)));

        notificationFormBloc.dispatch(
          SubmitNotificationPressed(
            title: _mockInputNotification.title,
            message: _mockInputNotification.message,
          ),
        );

        expectLater(
          notificationFormBloc.state,
          emitsInOrder([
            InitializeNotificationForm(
              isTitleValid: true,
              isMessageValid: true,
            ),
            LoadingNotificationForm(),
            SuccessNotificationForm(),
          ]),
        );
      });

      test("Failed", () {
        when(notificationRepository.createNotification(_mockInputNotification))
            .thenAnswer(
                (_) => Future.value(Response.createErrorResponse("Error")));

        notificationFormBloc.dispatch(
          SubmitNotificationPressed(
            title: _mockInputNotification.title,
            message: _mockInputNotification.message,
          ),
        );

        expectLater(
          notificationFormBloc.state,
          emitsInOrder([
            InitializeNotificationForm(
              isTitleValid: true,
              isMessageValid: true,
            ),
            LoadingNotificationForm(),
            FailedNotificationForm(errorMessage: "Error"),
          ]),
        );
      });
    });
  });
}
