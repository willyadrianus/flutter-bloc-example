import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Response<T> extends Equatable{
  static const int REQUEST_IN_PROGRESS = 88;
  static const int REQUEST_SUCCESS = 25;
  static const int REQUEST_SUCCESS_EMPTY = 26;
  ///when error happen, give user a way to retry the action
  static const int ERROR_SHOULD_RETRY = -25;
  ///when not authenticated, throw user to login page
  static const int ERROR_NOT_AUTHENTICATED = -26;
  ///When error happen, just do nothing
  static const int ERROR_DO_NOTHING = -27;

  int httpStatusCode;
  int result;
  String errorMessage;
  T data;

  Response({
    this.httpStatusCode,
    this.result,
    this.data,
    this.errorMessage = "",
  }) : super([
          httpStatusCode,
          result,
          data,
          errorMessage,
        ]);

  factory Response.createErrorResponse(String errorMessage) {
    return Response(
      result: ERROR_SHOULD_RETRY,
      errorMessage: errorMessage,
    );
  }

  factory Response.createLoadingResponse() {
    return Response(
      result: REQUEST_IN_PROGRESS,
    );
  }

  factory Response.createEmptySuccessResponse() {
    return Response(
      httpStatusCode: 200,
      result: REQUEST_SUCCESS_EMPTY,
    );
  }

  factory Response.createSuccessResponse(T data) {
    return Response(
      httpStatusCode: 200,
      result: REQUEST_SUCCESS,
      data: data,
    );
  }

  factory Response.modifyData({@required Response response, @required T data}) {
    return Response(
      httpStatusCode: response.httpStatusCode,
      result: response.result,
      errorMessage: response.errorMessage,
      data: data,
    );
  }

  bool get isLoading => result == REQUEST_IN_PROGRESS;

  bool get isSuccess => result == REQUEST_SUCCESS;

  bool get isEmptySuccess => result == REQUEST_SUCCESS_EMPTY;

  bool get isError =>
      result == ERROR_SHOULD_RETRY ||
      result == ERROR_DO_NOTHING ||
      result == ERROR_NOT_AUTHENTICATED;
}
