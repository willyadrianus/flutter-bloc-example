import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_example/bloc/notification/notification_bundle.dart';
import 'package:flutter_bloc_example/bloc/notification_form/notification_form_bundle.dart';
import 'package:flutter_bloc_example/model/notification/notification_message.dart';
import 'package:flutter_bloc_example/repository/notification_repository.dart';
import 'package:flutter_bloc_example/screens/notification_form_page.dart';
import 'package:flutter_bloc_example/util/device.dart';
import 'package:flutter_bloc_example/util/font_size.dart';
import 'package:flutter_bloc_example/util/resize.dart';
import 'package:flutter_bloc_example/widget/layout_wrapper.dart';
import 'package:flutter_bloc_example/widget/load_more.dart';
import 'package:flutter_bloc_example/widget/space.dart';

class NotificationPage extends StatefulWidget {
  @override
  State createState() => _State();
}

class _State extends State<NotificationPage> {
  NotificationBloc _notificationBloc;

  @override
  void initState() {
    super.initState();
    _notificationBloc = BlocProvider.of<NotificationBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Device.initialize(MediaQuery.of(context).size);
    FontSize.initialize();

    return Scaffold(
      appBar: _buildAppbar(),
      body: _buildBody(),
      floatingActionButton: _buildFAB(),
    );
  }

  Widget _buildAppbar() {
    return AppBar(
      automaticallyImplyLeading: false,
      title: Text(
        "Notification",
        style: TextStyle(
          fontSize: FontSize.title,
        ),
      ),
    );
  }

  Widget _buildBody() {
    return _buildNotification();
  }

  Widget _buildFAB() {
    return FloatingActionButton(
      onPressed: () async {
        return await Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => RepositoryProvider<NotificationRepository>(
              builder: (context) => NotificationRepository(),
              child: BlocProvider<NotificationFormBloc>(
                builder: (context) => NotificationFormBloc(
                    notificationRepository:
                        RepositoryProvider.of<NotificationRepository>(context)),
                child: NotificationFormPage(),
              ),
            ),
          ),
        );
      },
      child: Icon(
        Icons.add,
        color: Colors.white,
        size: Resize.of(20),
      ),
    );
  }

  Widget _buildNotification() {
    return BlocBuilder<NotificationBloc, NotificationState>(
      builder: (context, state) {
        if (state is SuccessNotification) {
          return _buildSuccessNotification(state);
        } else if (state is EmptySuccessNotification) {
          return _buildEmptySuccessNotification();
        } else if (state is FailedNotification) {
          return _buildFailedNotification(state);
        } else {
          return _buildLoadingNotification();
        }
      },
    );
  }

  Widget _buildSuccessNotification(SuccessNotification state) {
    return RefreshIndicator(
      onRefresh: () async => _reloadNotification(),
      child: LoadMore(
        onLoadMore: _onLoadMoreNotification,
        status: _notificationBloc.statusLoadMore,
        child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: _notificationBloc.notifications.length,
          itemBuilder: (context, index) {
            NotificationMessage notification =
            _notificationBloc.notifications[index];
            return ListTile(
              title: Text(
                index.toString() + " " + notification.title,
                style: TextStyle(
                  fontSize: FontSize.subtitle,
                ),
              ),
              subtitle: Text(
                notification.message,
                style: TextStyle(
                  fontSize: FontSize.standard,
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildEmptySuccessNotification() {
    return Center(
      child: Column(
        children: <Widget>[
          Icon(
            Icons.warning,
            color: Colors.yellow,
            size: Resize.of(50),
          ),
          Space.of(5),
          Text(
            "Tidak ada Notifikasi",
            style: TextStyle(
              fontSize: FontSize.subtitle,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFailedNotification(FailedNotification state) {
    return LayoutWrapper(
      children: <Widget>[
        GestureDetector(
          onTap: _reloadNotification,
          child: Center(
            child: Column(
              children: <Widget>[
                Icon(
                  Icons.error,
                  color: Colors.red,
                  size: Resize.of(50),
                ),
                Space.of(5),
                Text(
                  state.errorMessage,
                  style: TextStyle(
                    fontSize: FontSize.standard,
                  ),
                ),
                Space.of(5),
                Text(
                  "Tekan untuk Memuat Ulang",
                  style: TextStyle(
                    fontSize: FontSize.subtitle,
                    color: Colors.blue,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLoadingNotification() {
    return LayoutWrapper(
      children: <Widget>[
        Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Colors.blue),
          ),
        ),
      ],
    );
  }

  Future<void> _reloadNotification() async {
    _notificationBloc.dispatch(ReloadNotification());
  }

  Future<void> _onLoadMoreNotification() async {
    _notificationBloc
        .dispatch(LoadMoreNotification());
  }
}
