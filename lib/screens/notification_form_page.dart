import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_example/bloc/notification_form/notification_form_bundle.dart';
import 'package:flutter_bloc_example/util/font_size.dart';
import 'package:flutter_bloc_example/util/resize.dart';
import 'package:flutter_bloc_example/widget/layout_wrapper.dart';
import 'package:flutter_bloc_example/widget/snackbar_builder.dart';
import 'package:flutter_bloc_example/widget/space.dart';

class NotificationFormPage extends StatefulWidget {
  @override
  State createState() => _State();
}

class _State extends State<NotificationFormPage> {
  NotificationFormBloc _bloc;

  final _titleController = TextEditingController();
  final _messageController = TextEditingController();
  final _focusMessage = FocusNode();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<NotificationFormBloc>(context);
    _titleController.addListener(_onTitleChanged);
    _messageController.addListener(_onMessageChanged);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: _buildAppbar(),
      body: _buildBody(),
    );
  }

  Widget _buildAppbar() {
    return AppBar(
      title: Text(
        "Notification Form",
        style: TextStyle(
          fontSize: FontSize.title,
        ),
      ),
    );
  }

  Widget _buildBody() {
    return BlocListener<NotificationFormBloc, NotificationFormState>(
      listener: (context, state) {
        if (state is SuccessNotificationForm) {
          Navigator.of(context).pop();
        }
        else if (state is FailedNotificationForm) {
          _scaffoldKey.currentState.showSnackBar(
            SnackBarBuilder(
              message: "${state.errorMessage}",
              actionLabel: "OK",
            ),
          );
        }
      },
      child: BlocBuilder<NotificationFormBloc, NotificationFormState>(
        builder: (context, state) {
          return Stack(
            children: <Widget>[
              _buildNotificationForm(state),
              state is LoadingNotificationForm
                  ? _buildLoadingNotificationForm()
                  : Container(),
            ],
          );
        },
      ),
    );
  }

  Widget _buildNotificationForm(NotificationFormState state) {
    return LayoutWrapper(
      children: <Widget>[
        Space.of(5),
        _buildFormTitle(state),
        Space.of(5),
        _buildFormMessage(state),
        Space.of(5),
        _buildSubmitButton(state),
        Space.of(5),
      ],
    );
  }

  Widget _buildFormTitle(NotificationFormState state) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: Resize.of(10),
      ),
      child: TextFormField(
        autovalidate: true,
        autocorrect: false,
        style: TextStyle(fontSize: FontSize.standard),
        decoration: InputDecoration(
          labelText: "Judul",
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _titleController,
        validator: (_) {
          return !state.isTitleValid ? 'Judul minimal 3 Huruf' : null;
        },
        onFieldSubmitted: (_) {
          FocusScope.of(context).requestFocus(_focusMessage);
        },
      ),
    );
  }

  Widget _buildFormMessage(NotificationFormState state) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: Resize.of(10),
      ),
      child: TextFormField(
        autovalidate: true,
        autocorrect: false,
        style: TextStyle(fontSize: FontSize.standard),
        decoration: InputDecoration(
          labelText: "Pesan",
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        controller: _messageController,
        focusNode: _focusMessage,
        validator: (_) {
          return !state.isMessageValid ? 'Pesan tidak boleh kosong' : null;
        },
      ),
    );
  }

  Widget _buildSubmitButton(NotificationFormState state) {
    return Expanded(
      child: Container(
        alignment: Alignment(0, 1),
        child: RaisedButton(
          elevation: Resize.of(5),
          color: _isSubmitButtonEnabled(state) ? Colors.green : Colors.grey,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
            Resize.of(20),
          )),
          child: Text(
            'Buat Pesan Notifikasi',
            style: TextStyle(fontSize: FontSize.title, color: Colors.grey[100]),
          ),
          onPressed: () =>
              _isSubmitButtonEnabled(state) ? _onSubmitPressed() : null,
        ),
      ),
    );
  }

  Widget _buildLoadingNotificationForm() {
    return SizedBox.expand(
      child: Container(
        color: Color.fromARGB(125, 0, 0, 0),
        child: Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Colors.blue),
          ),
        ),
      ),
    );
  }

  void _onTitleChanged() {
    _bloc.dispatch(TitleChanged(title: _titleController.text));
  }

  void _onMessageChanged() {
    _bloc.dispatch(MessageChanged(message: _messageController.text));
  }

  bool _isSubmitButtonEnabled(NotificationFormState state) =>
      state.isFormValid && _isPopulated && state is! LoadingNotificationForm;

  bool get _isPopulated =>
      _titleController.text.isNotEmpty && _messageController.text.isNotEmpty;

  void _onSubmitPressed() {
    _bloc.dispatch(
      SubmitNotificationPressed(
        title: _titleController.text,
        message: _messageController.text,
      ),
    );
  }
}
