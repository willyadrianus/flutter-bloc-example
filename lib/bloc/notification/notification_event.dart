import 'package:equatable/equatable.dart';

abstract class NotificationEvent extends Equatable {
  NotificationEvent([List props = const []]) : super(props);
}

class FirstLoadNotification extends NotificationEvent {
  @override
  String toString() => "FirstLoadNotification";
}

class ReloadNotification extends NotificationEvent {
  @override
  String toString() => "ReloadNotification";
}

class LoadMoreNotification extends NotificationEvent {
  @override
  String toString() => "LoadMoreNotification";
}