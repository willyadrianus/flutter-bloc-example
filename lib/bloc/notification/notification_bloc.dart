import 'package:bloc/bloc.dart';
import 'package:flutter_bloc_example/model/filter.dart';
import 'package:flutter_bloc_example/model/notification/notification_message.dart';
import 'package:flutter_bloc_example/provider/load_more_bloc.dart';
import 'package:flutter_bloc_example/repository/notification_repository.dart';
import 'package:flutter_bloc_example/widget/load_more.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import 'notification_event.dart';
import 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> with LoadMoreBloc {
  List<NotificationMessage> notifications = [];
  final NotificationRepository notificationRepository;

  NotificationBloc({@required this.notificationRepository});

  @override
  NotificationState get initialState => LoadingNotification();

  @override
  Stream<NotificationState> transformEvents(
      Stream<NotificationEvent> events,
      Stream<NotificationState> Function(NotificationEvent event) next,
      ) {
    final observableStream = events as Observable<NotificationEvent>;
    final nonDebounceStream = observableStream.where((event) {
      return (event is! LoadMoreNotification);
    });
    final debounceStream = observableStream.where((event) {
      return (event is LoadMoreNotification);
    }).debounceTime(Duration(milliseconds: 100));
    return super
        .transformEvents(nonDebounceStream.mergeWith([debounceStream]), next);
  }

  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if (event is FirstLoadNotification) {
      yield* _mapLoadNotificationToState();
    } else if (event is ReloadNotification) {
      yield* _mapLoadNotificationToState();
    } else if (event is LoadMoreNotification) {
      yield* _mapLoadMoreNotificationToState();
    }
  }

  Stream<NotificationState> _mapLoadNotificationToState() async* {
    yield LoadingNotification();
    final result = await notificationRepository.loadNotifications(
      filter: Filter(
        limit: 10,
      ),
    );
    if (result.isSuccess) {
      notifications.clear();
      notifications.addAll(result.data);
      yield SuccessNotification();
    } else if (result.isEmptySuccess) {
      yield EmptySuccessNotification();
    } else if (result.isError) {
      yield FailedNotification(
          errorMessage: result.errorMessage?.isNotEmpty ?? false
              ? result.errorMessage
              : "Gagal Mengambil Data");
    }
  }

  Stream<NotificationState> _mapLoadMoreNotificationToState() async* {
    statusLoadMore = LoadMoreStatus.LOADING;
    yield LoadMoreLoadingNotification();
    final result = await notificationRepository.loadNotifications(
        filter: loadMoreFilter);
    if (result.isSuccess) {
      notifications.addAll(result.data);
      loadMoreFilter = Filter.copy(
        loadMoreFilter,
        offset: loadMoreFilter.offset + result.data.length,
      );
      statusLoadMore = LoadMoreStatus.IDLE;
      yield LoadMoreSuccessNotification();
    } else if (result.isEmptySuccess) {
      statusLoadMore = LoadMoreStatus.FINISH;
      yield LoadMoreEmptySuccessNotification();
    } else if (result.isError) {
      statusLoadMore = LoadMoreStatus.FAILED;
      yield LoadMoreFailedNotification(
          errorMessage: result.errorMessage?.isNotEmpty ?? false
              ? result.errorMessage
              : "Gagal Mengambil Data");
    }
  }
}
