import 'dart:async';

import 'package:flutter_bloc_example/model/filter.dart';
import 'package:flutter_bloc_example/model/notification/notification_message.dart';
import 'package:flutter_bloc_example/model/response.dart';
import 'package:flutter_bloc_example/service/service.dart';

class NotificationService extends Service {
  NotificationMessage _mockNotification = NotificationMessage(
    id: 1,
    title: "Notifikasi 1",
    message: "Ini adalah Pesan Notifikasi 1",
  );

  List<NotificationMessage> get _mockNotifications => [
        _mockNotification,
        _mockNotification,
        _mockNotification,
        _mockNotification,
        _mockNotification,
        _mockNotification,
        _mockNotification,
        _mockNotification,
        _mockNotification,
        _mockNotification,
        _mockNotification,
  ];

  Future<Response<List<NotificationMessage>>> loadNotifications({
    Filter filter,
  }) async {
    await Future.delayed(Duration(seconds: 1));
    return Response.createSuccessResponse(
        _mockNotifications); // Response Success
    return Response.createEmptySuccessResponse(); // Response Empty Success
    return Response.createErrorResponse(
        "Maaf, Server sedang Maintenance"); // Response Failed
  }

  Future<Response<NotificationMessage>> createNotification(
    NotificationMessage notificationMessage,
  ) async {
    await Future.delayed(Duration(seconds: 1));
    return Response.createSuccessResponse(
      NotificationMessage(
        id: 1,
        title: notificationMessage.title,
        message: notificationMessage.message,
      ),
    ); // Response Success
    return Response.createErrorResponse(
        "Maaf, Server sedang Maintenance"); // Response Failed
  }
}
