import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_example/model/filter.dart';
import 'package:json_annotation/json_annotation.dart';

part 'notification_message.g.dart';

@JsonSerializable()
class NotificationMessage extends Equatable{
  final int id;
  final String title;
  final String message;

  NotificationMessage({this.id, this.title, this.message}) : super([id, title, message]);

  factory NotificationMessage.fromJson(Map<String, dynamic> json) =>
      _$NotificationFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationToJson(this);

  static Map loadNotificationsAPI({Filter filter = const Filter()}) {
    return {
      "name": "loadNotificationsAPI",
      "data": {
        "offset": filter.offset,
        "limit": filter.limit,
        "order_by": filter.orderBy,
      }
    };
  }

  static List<NotificationMessage> fromAPI(List<dynamic> jsonDecodeResult) {
    List<NotificationMessage> notifications = [];
    if (jsonDecodeResult == null) return notifications;
    for (final map in jsonDecodeResult) {
      if (map != null) {
        final notification = NotificationMessage.fromJson(map);
        notifications.add(notification);
      }
    }
    return notifications;
  }
}