import 'dart:async';
import 'dart:convert';

import 'package:flutter_bloc_example/model/filter.dart';
import 'package:flutter_bloc_example/model/notification/notification_message.dart';
import 'package:flutter_bloc_example/model/response.dart';
import 'package:flutter_bloc_example/service/service.dart';

class NotificationService extends Service {
  Future<Response<List<NotificationMessage>>> loadNotifications({
    Filter filter,
  }) async {
    final String encodedJson = json.encode(NotificationMessage.loadNotificationsAPI(
      filter: filter,
    ));
    final Response<dynamic> response = await postRequest(encodedJson);

    List<NotificationMessage> data = [];
    if (response.isSuccess) data = NotificationMessage.fromAPI(response.data);
    return Response.modifyData(
      response: response,
      data: data,
    );
  }
}
