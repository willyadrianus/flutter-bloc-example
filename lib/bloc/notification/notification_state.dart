import 'package:equatable/equatable.dart';

abstract class NotificationState extends Equatable{
  NotificationState([List props = const []]) : super(props);
}

class LoadingNotification extends NotificationState{
  @override
  String toString() => "LoadingNotification";
}

class SuccessNotification extends NotificationState{
  SuccessNotification([List props = const []]) : super(props);

  @override
  String toString() => "SuccessNotification";
}

class EmptySuccessNotification extends NotificationState{
  @override
  String toString() => "EmptySuccessNotification";
}

class FailedNotification extends NotificationState{
  final String errorMessage;

  FailedNotification({this.errorMessage}) : super([errorMessage]);

  @override
  String toString() => "FailedNotification { errorMessage: $errorMessage } ";
}

class LoadMoreLoadingNotification extends SuccessNotification{
  @override
  String toString() => "LoadMoreLoadingNotification";
}

class LoadMoreSuccessNotification extends SuccessNotification{
  @override
  String toString() => "LoadMoreSuccessNotification";
}

class LoadMoreEmptySuccessNotification extends SuccessNotification{
  @override
  String toString() => "LoadMoreEmptySuccessNotification";
}

class LoadMoreFailedNotification extends SuccessNotification{
  final String errorMessage;

  LoadMoreFailedNotification({this.errorMessage}) : super([errorMessage]);

  @override
  String toString() => "LoadMoreFailedNotification { errorMessage: $errorMessage } ";
}