import 'package:equatable/equatable.dart';

abstract class NotificationFormState extends Equatable {
  bool isTitleValid;
  bool isMessageValid;

  bool get isFormValid => isTitleValid && isMessageValid;

  NotificationFormState({
    this.isTitleValid,
    this.isMessageValid,
    List props = const [],
  }) : super([
          isTitleValid,
          isMessageValid,
          props,
        ]);

  NotificationFormState update({
    bool isTitleValid,
    bool isMessageValid,
  }) {
    return copyWith(
      isTitleValid: isTitleValid ?? this.isTitleValid,
      isMessageValid: isMessageValid ?? this.isMessageValid,
    );
  }

  InitializeNotificationForm copyWith({
    bool isTitleValid,
    bool isMessageValid,
  }) {
    return InitializeNotificationForm(
      isTitleValid: isTitleValid,
      isMessageValid: isMessageValid,
    );
  }
}

class InitializeNotificationForm extends NotificationFormState {
  InitializeNotificationForm({
    bool isTitleValid,
    bool isMessageValid,
  }) : super(
          isTitleValid: isTitleValid,
          isMessageValid: isMessageValid,
        );

  @override
  String toString() => "InitializeNotificationForm ( isTitleValid: $isTitleValid, isMessageValid: $isMessageValid ) ";
}

class LoadingNotificationForm extends NotificationFormState {
  @override
  String toString() => "LoadingNotificationForm";
}

class SuccessNotificationForm extends NotificationFormState {
  @override
  String toString() => "SuccessNotificationForm";
}

class FailedNotificationForm extends NotificationFormState {
  final String errorMessage;

  FailedNotificationForm({this.errorMessage}) : super(props: [errorMessage]);

  @override
  String toString() => "FailedNotificationForm";
}
