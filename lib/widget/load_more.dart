import 'package:flutter/material.dart';
import 'package:flutter_bloc_example/util/font_size.dart';
import 'package:flutter_bloc_example/util/resize.dart';
import 'package:flutter_bloc_example/widget/space.dart';

/// Only for ListView and Sliverlist
/// GridView is not Tested
/// Idk how its works.. but it is working just trust me..
/// Upgradable from flutter loadmore 1.0.4

enum LoadMoreStatus {
  IDLE,
  LOADING,
  FAILED,
  FINISH,
}

typedef Future<void> LoadMoreCallBack();

class LoadMore extends StatelessWidget {
  final Widget child;
  final LoadMoreCallBack onLoadMore;
  final LoadMoreStatus status;
  final Widget onLoading;
  final Widget onFailed;
  final Widget onFinish;

  LoadMore({
    @required this.child,
    @required this.onLoadMore,
    this.status = LoadMoreStatus.IDLE,
    this.onLoading,
    this.onFailed,
    this.onFinish,
  });

  @override
  Widget build(BuildContext context) {
    if (onLoadMore == null) {
      return child;
    } else if (child is ListView) {
      return _buildListView(child);
    } else if (child is SliverList) {
      return _buildSliverList(child);
    } else if (child is GridView) {
      GridView gridView = child;
      return NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scrollInfo) {
          if (status != LoadMoreStatus.LOADING &&
              scrollInfo.metrics.pixels == scrollInfo.metrics.maxScrollExtent) {
            onLoadMore();
            return false;
          } else
            return true;
        },
        child: ListView(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: gridView.scrollDirection,
          children: <Widget>[
            child,
            Space.of(5),
            _buildStatusView(),
          ],
        ),
      );
    } else {
      return child;
    }
  }

  Widget _buildListView(ListView listView) {
    if (listView.childrenDelegate is SliverChildBuilderDelegate) {
      SliverChildBuilderDelegate delegate = listView.childrenDelegate;

      final viewCount = delegate.estimatedChildCount + 1;
      IndexedWidgetBuilder builder = (context, index) {
        if (index == viewCount - 1) {
          return _buildStatusView();
        }
        return delegate.builder(context, index);
      };

      return ListView.builder(
        itemBuilder: builder,
        addAutomaticKeepAlives: delegate.addAutomaticKeepAlives,
        addRepaintBoundaries: delegate.addRepaintBoundaries,
        addSemanticIndexes: delegate.addSemanticIndexes,
        dragStartBehavior: listView.dragStartBehavior,
        semanticChildCount: listView.semanticChildCount,
        itemCount: viewCount,
        cacheExtent: listView.cacheExtent,
        controller: listView.controller,
        itemExtent: listView.itemExtent,
        key: listView.key,
        padding: listView.padding,
        physics: listView.physics,
        primary: listView.primary,
        reverse: listView.reverse,
        scrollDirection: listView.scrollDirection,
        shrinkWrap: listView.shrinkWrap,
      );
    } else if (listView.childrenDelegate is SliverChildListDelegate) {
      SliverChildListDelegate delegate = listView.childrenDelegate;

      delegate.children.add(_buildStatusView());
      return ListView(
        children: delegate.children,
        addAutomaticKeepAlives: delegate.addAutomaticKeepAlives,
        addRepaintBoundaries: delegate.addRepaintBoundaries,
        cacheExtent: listView.cacheExtent,
        controller: listView.controller,
        itemExtent: listView.itemExtent,
        key: listView.key,
        padding: listView.padding,
        physics: listView.physics,
        primary: listView.primary,
        reverse: listView.reverse,
        scrollDirection: listView.scrollDirection,
        shrinkWrap: listView.shrinkWrap,
        addSemanticIndexes: delegate.addSemanticIndexes,
        dragStartBehavior: listView.dragStartBehavior,
        semanticChildCount: listView.semanticChildCount,
      );
    } else
      return listView;
  }

  Widget _buildSliverList(SliverList list) {
    final delegate = list.delegate;
    if (delegate == null) {
      return list;
    }

    if (delegate is SliverChildListDelegate) {
      return SliverList(
        delegate: null,
      );
    }

    if (delegate is SliverChildBuilderDelegate) {
      final viewCount = delegate.estimatedChildCount + 1;
      IndexedWidgetBuilder builder = (context, index) {
        if (index == viewCount - 1) {
          return _buildStatusView();
        }
        return delegate.builder(context, index);
      };

      return SliverList(
        delegate: SliverChildBuilderDelegate(
          builder,
          addAutomaticKeepAlives: delegate.addAutomaticKeepAlives,
          addRepaintBoundaries: delegate.addRepaintBoundaries,
          addSemanticIndexes: delegate.addSemanticIndexes,
          childCount: viewCount,
          semanticIndexCallback: delegate.semanticIndexCallback,
          semanticIndexOffset: delegate.semanticIndexOffset,
        ),
      );
    }

    if (delegate is SliverChildListDelegate) {
      delegate.children.add(_buildStatusView());
      return SliverList(
        delegate: SliverChildListDelegate(
          delegate.children,
          addAutomaticKeepAlives: delegate.addAutomaticKeepAlives,
          addRepaintBoundaries: delegate.addRepaintBoundaries,
          addSemanticIndexes: delegate.addSemanticIndexes,
          semanticIndexCallback: delegate.semanticIndexCallback,
          semanticIndexOffset: delegate.semanticIndexOffset,
        ),
      );
    }

    return list;
  }

  Widget _buildStatusView() {
    if (status != LoadMoreStatus.LOADING) {
      onLoadMore();
    }

    if (status == LoadMoreStatus.IDLE) {
      return Container(
        height: Resize.of(40),
      );
    } else if (status == LoadMoreStatus.LOADING) {
      return onLoading ??
          Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Colors.blue),
            ),
          );
    } else if (status == LoadMoreStatus.FAILED) {
      return GestureDetector(
        onTap: () async => await onLoadMore(),
        child: onFailed ??
            Center(
              child: Column(
                children: <Widget>[
                  Icon(
                    Icons.warning,
                    color: Colors.yellow,
                    size: Resize.of(50),
                  ),
                  Space.of(5),
                  Text(
                    "Gagal Memuat Data",
                    style: TextStyle(
                      fontSize: FontSize.standard,
                    ),
                  ),
                ],
              ),
            ),
      );
    } else if (status == LoadMoreStatus.FINISH) {
      return onFinish ??
          Center(
            child: Text(
              "Tidak ada lagi Data",
              style: TextStyle(
                fontSize: FontSize.standard,
              ),
            ),
          );
    } else {
      return Container();
    }
  }
}
