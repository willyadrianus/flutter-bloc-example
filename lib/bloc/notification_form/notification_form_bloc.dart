import 'package:bloc/bloc.dart';
import 'package:flutter_bloc_example/model/notification/notification_message.dart';
import 'package:flutter_bloc_example/repository/notification_repository.dart';
import 'package:flutter_bloc_example/validator/validators.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import 'notification_form_event.dart';
import 'notification_form_state.dart';

class NotificationFormBloc
    extends Bloc<NotificationFormEvent, NotificationFormState> {
  final NotificationRepository notificationRepository;

  NotificationFormBloc({@required this.notificationRepository})
      : assert(notificationRepository != null);

  @override
  NotificationFormState get initialState => InitializeNotificationForm(
        isTitleValid: true,
        isMessageValid: true,
      );

  @override
  Stream<NotificationFormState> transformEvents(
    Stream<NotificationFormEvent> events,
    Stream<NotificationFormState> Function(NotificationFormEvent event) next,
  ) {
    final observableStream = events as Observable<NotificationFormEvent>;
    final nonDebounceStream = observableStream.where((event) {
      return (event is! TitleChanged && event is! MessageChanged);
    });
    final debounceStream = observableStream.where((event) {
      return (event is TitleChanged || event is MessageChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super
        .transformEvents(nonDebounceStream.mergeWith([debounceStream]), next);
  }

  @override
  void onTransition(
      Transition<NotificationFormEvent, NotificationFormState> transition) {
    transition.nextState.isTitleValid ??= currentState.isTitleValid;
    transition.nextState.isMessageValid ??= currentState.isMessageValid;
    return super.onTransition(transition);
  }

  @override
  Stream<NotificationFormState> mapEventToState(
      NotificationFormEvent event) async* {
    if (event is TitleChanged) {
      yield* _mapTitleChangedToState(event.title);
    } else if (event is MessageChanged) {
      yield* _mapMessageChangedToState(event.message);
    } else if (event is SubmitNotificationPressed) {
      yield* _mapSubmitNotificationPressedToState(
        title: event.title,
        message: event.message,
      );
    }
  }

  Stream<NotificationFormState> _mapTitleChangedToState(String title) async* {
    yield currentState.update(
      isTitleValid: Validators.isLengthEqualThan(title, 3),
    );
  }

  Stream<NotificationFormState> _mapMessageChangedToState(
      String message) async* {
    yield currentState.update(
      isMessageValid: false,
    );
  }

  Stream<NotificationFormState> _mapSubmitNotificationPressedToState({
    String title,
    String message,
  }) async* {
    yield LoadingNotificationForm();
    final result = await notificationRepository.createNotification(
      NotificationMessage(
        title: title,
        message: message,
      ),
    );
    if (result.isSuccess) {
      yield SuccessNotificationForm();
    } else {
      yield FailedNotificationForm(
          errorMessage: result.errorMessage?.isNotEmpty ?? false
              ? result.errorMessage
              : "Gagal Mengambil Data");
    }
  }
}
