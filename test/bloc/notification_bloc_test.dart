import 'package:flutter_bloc_example/bloc/notification/notification_bundle.dart';
import 'package:flutter_bloc_example/model/notification/notification_message.dart';
import 'package:flutter_bloc_example/model/response.dart';
import 'package:flutter_bloc_example/repository/notification_repository.dart';
import 'package:mockito/mockito.dart';
import 'package:test_api/test_api.dart';

class NotificationRepositoryMock extends Mock
    implements NotificationRepository {}

void main() {
  group("Notification Bloc", () {
    NotificationBloc notificationBloc;
    NotificationRepository notificationRepository;
    NotificationMessage _mockNotification = NotificationMessage(
      id: 1,
      title: "Hi",
      message: "Test aja",
    );

    setUp(() {
      notificationRepository = NotificationRepositoryMock();
      notificationBloc =
          NotificationBloc(notificationRepository: notificationRepository);
    });

    test("Initial State is LoadingNotification", () {
      expect(
        notificationBloc.initialState,
        LoadingNotification(),
      );
    });

    group("FirstLoadNotification Event", () {
      test("Success", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer((_) =>
            Future.value(Response.createSuccessResponse([_mockNotification])));

        notificationBloc.dispatch(FirstLoadNotification());

        await expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            SuccessNotification(),
          ]),
        );

        expectLater(notificationBloc.notifications, []);

      });

      test("Empty Success", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer(
            (_) => Future.value(Response.createEmptySuccessResponse()));

        expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            EmptySuccessNotification(),
          ]),
        );

        notificationBloc.dispatch(FirstLoadNotification());
      });

      test("Failed", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer(
            (_) => Future.value(Response.createErrorResponse("Error")));

        expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            FailedNotification(errorMessage: "Error"),
          ]),
        );

        notificationBloc.dispatch(FirstLoadNotification());
      });
    });

    group("ReloadNotification Event", () {
      test("Success", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer((_) =>
            Future.value(Response.createSuccessResponse([_mockNotification])));

        expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            SuccessNotification(),
          ]),
        );

        notificationBloc.dispatch(ReloadNotification());
      });

      test("Empty Success", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer(
            (_) => Future.value(Response.createEmptySuccessResponse()));

        expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            EmptySuccessNotification(),
          ]),
        );

        notificationBloc.dispatch(ReloadNotification());
      });

      test("Failed", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer(
            (_) => Future.value(Response.createErrorResponse("Error")));

        expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            FailedNotification(errorMessage: "Error"),
          ]),
        );

        notificationBloc.dispatch(ReloadNotification());
      });
    });

    group("LoadMoreNotification Event", () {
      test("Success", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer((_) =>
            Future.value(Response.createSuccessResponse([_mockNotification])));

        expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            LoadMoreLoadingNotification(),
            LoadMoreSuccessNotification(),
          ]),
        );

        notificationBloc.dispatch(LoadMoreNotification());
      });

      test("Empty Success", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer(
                (_) => Future.value(Response.createEmptySuccessResponse()));

        expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            LoadMoreLoadingNotification(),
            LoadMoreEmptySuccessNotification(),
          ]),
        );

        notificationBloc.dispatch(LoadMoreNotification());
      });

      test("Failed", () async {
        when(notificationRepository.loadNotifications(
          filter: anyNamed("filter"),
        )).thenAnswer(
                (_) => Future.value(Response.createErrorResponse("Error")));

        expectLater(
          notificationBloc.state,
          emitsInOrder([
            LoadingNotification(),
            LoadMoreLoadingNotification(),
            LoadMoreFailedNotification(errorMessage: "Error"),
          ]),
        );

        notificationBloc.dispatch(LoadMoreNotification());
      });
    });
  });
}
