class Validators {
  static final RegExp _emailRegExp = RegExp(
    r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$',
  );

  static isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  static isValidPassword(String password) {
    return password.length >= 8;
  }

  static final RegExp _phoneRegExp = RegExp(
      '^(08|\\+62)+[0-9]{7,}\$', caseSensitive: false, multiLine: false
  );

  static isValidPhone(String phone){
    return _phoneRegExp.hasMatch(phone);
  }

  static isMatched(String one, String two){
    return one == two;
  }

  static isTrue(bool one){
    return one;
  }

  static isNotEmpty(dynamic one){
    return one != null && ((one is! String) || (one is String && one.isNotEmpty));
  }

  static isLengthEqualThan(String text, int length){
    return text != null && text.isNotEmpty && text.length >= length;
  }
}