import 'package:flutter_bloc_example/mockservice/notification_service.dart';
import 'package:flutter_bloc_example/model/filter.dart';
import 'package:flutter_bloc_example/model/notification/notification_message.dart';
import 'package:flutter_bloc_example/model/response.dart';

class NotificationRepository {
  final NotificationService _notificationService = NotificationService();

  Future<Response<List<NotificationMessage>>> loadNotifications({
    Filter filter,
  }) async =>
      await _notificationService.loadNotifications(
        filter: filter,
      );

  Future<Response<NotificationMessage>> createNotification(
    NotificationMessage notificationMessage,
  ) async =>
      await _notificationService.createNotification(
        notificationMessage,
      );
}
