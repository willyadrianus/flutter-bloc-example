class Filter {
  final int offset;
  final int limit;
  final String orderBy;

  const Filter({
    this.offset = 0,
    this.limit = 10,
    this.orderBy = "id desc",
  });

  factory Filter.copy(Filter filter, {int offset, int limit, String orderBy}) {
    return Filter(
      offset: offset ?? filter.offset,
      limit: limit ?? filter.limit,
      orderBy: orderBy ?? filter.orderBy,
    );
  }

}
