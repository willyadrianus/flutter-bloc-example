import 'resize.dart';

/// FontSize is Util
/// digunakan untuk mendapatkan ukuran font sesuai tinggi/lebar device
/// [_field] dapat diubah sesuai kebutuhan
/// [_standard] adalah ukuran standard font
/// [_title] adalah ukuran title font / big font
/// [_minSize] dan [_maxSize] adalah ukuran minimal dan maksimal font
///
/// Readme:
/// Wajib "initialize()" saat halaman awal sesudah mendapatkan informasi device
///
/// Test:
/// menggunakan Resize.dart
///
/// Dependency:
/// Resize.dart, Device.dart

class FontSize{

  static double small = 0;
  static double standard = 0;
  static double subtitle = 0;
  static double title = 0;

  static final double _small = 11;
  static final double _standard = 14;
  static final double _subtitle = 16;
  static final double _title = 20;
  static final double _minSize = 8;
  static final double _maxSize = 9999;

  static initialize(){
    small = Resize.of(_small);
    standard = Resize.of(_standard);
    subtitle = Resize.of(_subtitle);
    title = Resize.of(_title);

    if (_minSize > small) small = _minSize;
    else if (_maxSize < small) small = _maxSize;

    if (_minSize > standard) standard = _minSize;
    else if (_maxSize < standard) standard = _maxSize;

    if (_minSize > subtitle) subtitle = _minSize;
    else if (_maxSize < subtitle) subtitle = _maxSize;

    if (_minSize > title) title = _minSize;
    else if (_maxSize < title) title = _maxSize;
  }

  static of(double value) => Resize.of(value, minSize: _minSize, maxSize: _maxSize);

  static value(double value) => of(value);

}