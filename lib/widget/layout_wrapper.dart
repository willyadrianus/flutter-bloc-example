import 'package:flutter/material.dart';

/// Widget untuk membungkus Form
/// Digunakan untuk memaksilkan seluruh layar device,
/// apabila tinggi form melebihi tinggi device,
/// maka secara otomatis form dapat di scroll
///
/// Parameter :
/// [padding], [children]

class LayoutWrapper extends StatelessWidget {
  final EdgeInsetsGeometry padding;
  final List<Widget> children;
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;

  LayoutWrapper(
      {this.crossAxisAlignment = CrossAxisAlignment.center,
      this.mainAxisAlignment = MainAxisAlignment.center,
      this.padding,
      this.children});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints boxConstraints) {
          return SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: boxConstraints.maxHeight),
              child: IntrinsicHeight(
                child: Column(
                  mainAxisAlignment: mainAxisAlignment,
                  crossAxisAlignment: crossAxisAlignment,
                  children: children,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
