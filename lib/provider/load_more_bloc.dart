import 'package:flutter_bloc_example/model/filter.dart';
import 'package:flutter_bloc_example/widget/load_more.dart';

class LoadMoreBloc {
  LoadMoreStatus statusLoadMore = LoadMoreStatus.IDLE;
  Filter loadMoreFilter = Filter(offset: 0, limit: 10);
}