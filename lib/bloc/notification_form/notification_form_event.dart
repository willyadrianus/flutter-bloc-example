import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class NotificationFormEvent extends Equatable {
  NotificationFormEvent([List props = const []]) : super(props);
}

class TitleChanged extends NotificationFormEvent {
  final String title;

  TitleChanged({@required this.title}) : super([title]);

  @override
  String toString() => 'TitleChanged { title: $title }';
}

class MessageChanged extends NotificationFormEvent {
  final String message;

  MessageChanged({@required this.message}) : super([message]);

  @override
  String toString() => 'MessageChanged { message: $message }';
}

class SubmitNotificationPressed extends NotificationFormEvent {
  final String title;
  final String message;

  SubmitNotificationPressed({
    @required this.title,
    @required this.message,
  }) : super([title, message]);

  @override
  String toString() =>
      'SubmitNotificationPressed { title: $title, message: $message }';
}
